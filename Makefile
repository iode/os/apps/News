BUILD    = flutter build apk --release
RUN      = flutter run
DEV      = --flavor dev --target lib/main.dart
PROD     = --flavor prod --target lib/main_prod.dart
TARGET   = --target-platform android-arm64

analyze:
	@flutter analyze lib

build_dev:
	$(BUILD) $(DEV) $(TARGET)

build_prod:
	$(BUILD) $(PROD) $(TARGET)

run:
	adb shell su -c setenforce 0
	$(RUN) $(DEV)

run_prod:
	$(RUN) $(PROD)

format:
	@dart format --set-exit-if-changed -o none lib test

test:
	@flutter test --coverage --test-randomize-ordering-seed random

coverage:
	genhtml coverage/lcov.info -o coverage

view_cov:
	open coverage/index.html

clean:
	@flutter clean

packages:
	@flutter packages get

unused:
	@echo "Looking for unused files..."
	@flutter pub run dart_code_metrics:metrics check-unused-files lib
	@echo "Looking for unused l10n..."
	@flutter pub run dart_code_metrics:metrics check-unused-l10n lib
	@echo "Looking for unused code..."
	@flutter pub run dart_code_metrics:metrics check-unused-code lib
