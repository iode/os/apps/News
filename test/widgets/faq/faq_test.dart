import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:news/faq/presenter/faq.dart';
import 'package:news/faq/presenter/provider.dart';
import 'package:news/shared/component/error_text.dart';
import 'package:news/shared/component/no_connection.dart';

import '../faker.dart';

void main() {
  late IodeNewsFaker _faker;
  const lang = 'fr';

  setUp(() => _faker = IodeNewsFaker());

  group('FAQ page', () {
    testWidgets('renders', (t) async {
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            faqListProvider(lang).overrideWithValue(
              const AsyncValue.loading(),
            ),
          ],
          child: _faker.materialWrapper(const FAQPage()),
        ),
      );
      expect(find.byType(FAQPage), findsOneWidget);
    });

    testWidgets('loading', (t) async {
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            faqListProvider(lang).overrideWithValue(
              const AsyncValue.loading(),
            ),
          ],
          child: _faker.materialWrapper(const FAQPage()),
        ),
      );
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
    });

    testWidgets('with no internet error', (t) async {
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            faqListProvider(lang).overrideWithValue(
              const AsyncValue.error(SocketException('error')),
            ),
          ],
          child: _faker.materialWrapper(const FAQPage()),
        ),
      );
      expect(find.byType(NoConnectionWidget), findsOneWidget);
    });

    testWidgets('with error', (t) async {
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            faqListProvider(lang).overrideWithValue(
              AsyncValue.error(Exception('error')),
            ),
          ],
          child: _faker.materialWrapper(const FAQPage()),
        ),
      );
      expect(find.byType(ErrorTextWidget), findsOneWidget);
    });

    testWidgets('empty', (t) async {
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            faqListProvider(lang).overrideWithValue(
              const AsyncValue.data({}),
            ),
          ],
          child: _faker.materialWrapper(const FAQPage()),
        ),
      );
      expect(find.text('Aucune question à afficher'), findsOneWidget);
    });

    testWidgets('with data but empty questions', (t) async {
      final data = _faker.faqData(questionsAmount: 0);
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            faqListProvider(lang).overrideWithValue(
              AsyncValue.data(data),
            ),
          ],
          child: _faker.materialWrapper(const FAQPage()),
        ),
      );

      // Renders FaqTile widget
      expect(
        t.firstWidget(find.byType(FaqTile)),
        isA<FaqTile>()
            .having((f) => f.category, 'category', data.keys.first)
            .having((f) => f.questions, 'questions', data.values.first),
      );
      // Renders category ExpansionTile widget
      expect(find.byType(ExpansionTile), findsOneWidget);
      await t.tap(find.byType(ExpansionTile));
      await t.pumpAndSettle();
      expect(find.text('Aucune question dans cette catégorie'), findsOneWidget);
    });

    testWidgets('with data', (t) async {
      final data = _faker.faqData();
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            faqListProvider(lang).overrideWithValue(
              AsyncValue.data(data),
            ),
          ],
          child: _faker.materialWrapper(const FAQPage()),
        ),
      );
      // Renders FaqTile widget
      expect(
        t.firstWidget(find.byType(FaqTile)),
        isA<FaqTile>()
            .having((f) => f.category, 'category', data.keys.first)
            .having((f) => f.questions, 'questions', data.values.first),
      );
      // Renders category ExpansionTile widget
      expect(find.byType(ExpansionTile), findsOneWidget);
      await t.tap(find.byType(ExpansionTile));
      await t.pumpAndSettle();
      // We only have one faq for this category (see [_faker.faqData])
      final faq = data.values.first.first;
      expect(find.text(faq.title), findsOneWidget);
      final faqExpansionTile = find.byKey(Key(faq.title));
      expect(faqExpansionTile, findsOneWidget);
      await t.tap(faqExpansionTile);
      await t.pumpAndSettle();
      expect(
        t.firstWidget(find.byType(MarkdownBody)),
        isA<MarkdownBody>()
            .having((md) => md.data, 'markdown.data', faq.description),
      );
    });
  });
}
