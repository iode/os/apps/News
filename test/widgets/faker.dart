import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:news/faq/domain/faq.dart';
import 'package:news/faq/domain/faq_category.dart';
import 'package:news/l10n/l10n.dart';
import 'package:news/news/domain/post.dart';
import 'package:news/news/domain/post_category.dart';

class IodeNewsFaker {
  final faker = Faker();

  Widget materialWrapper(Widget child) => MaterialApp(
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: const [Locale('fr')],
        home: child,
      );

  FAQCategory faqCategory() => FAQCategory
      .values[faker.randomGenerator.integer(FAQCategory.values.length)];

  FAQ faq({
    FAQCategory? category,
  }) =>
      FAQ(
        category: category ?? faqCategory(),
        devices: [],
        title: faker.lorem.words(3).join(' '),
        description: faker.lorem.sentence(),
      );

  Map<FAQCategory, Set<FAQ>> faqData({
    int questionsAmount = 1,
  }) {
    final category = faqCategory();
    return {
      category: {
        for (var i = 0; i < questionsAmount; i++) faq(category: category),
      }
    };
  }

  PostCategory postCategory() => PostCategory
      .values[faker.randomGenerator.integer(PostCategory.values.length)];

  Post post() => Post(
        id: faker.guid.guid(),
        category: postCategory(),
        devices: [],
        title: faker.lorem.sentence(),
        description: faker.lorem.sentences(4).join('\n'),
        date: faker.date.dateTime(),
      );

  Set<Post> newsData({int postsAmount = 1}) => {
        for (var i = 0; i < postsAmount; i++) post(),
      };
}
