import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:news/news/domain/post_category.dart';
import 'package:news/news/presenter/news.dart';
import 'package:news/news/presenter/post_card.dart';
import 'package:news/news/presenter/provider.dart';
import 'package:news/shared/component/error_text.dart';
import 'package:news/shared/component/no_connection.dart';

import '../faker.dart';

void main() {
  late IodeNewsFaker _faker;
  const lang = 'fr';

  setUp(() => _faker = IodeNewsFaker());

  group('News page', () {
    testWidgets('renders', (t) async {
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            postListProvider(lang).overrideWithValue(
              const AsyncValue.loading(),
            )
          ],
          child: _faker.materialWrapper(const NewsPage()),
        ),
      );
      expect(find.byType(NewsPage), findsOneWidget);
    });

    testWidgets('loading', (t) async {
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            postListProvider(lang).overrideWithValue(
              const AsyncValue.loading(),
            )
          ],
          child: _faker.materialWrapper(const NewsPage()),
        ),
      );
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
    });

    testWidgets('with no internet error', (t) async {
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            postListProvider(lang).overrideWithValue(
              const AsyncValue.error(SocketException('error')),
            ),
          ],
          child: _faker.materialWrapper(const NewsPage()),
        ),
      );
      expect(find.byType(NoConnectionWidget), findsOneWidget);
    });

    testWidgets('with error', (t) async {
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            postListProvider(lang).overrideWithValue(
              AsyncValue.error(Exception('error')),
            ),
          ],
          child: _faker.materialWrapper(const NewsPage()),
        ),
      );
      expect(find.byType(ErrorTextWidget), findsOneWidget);
    });

    testWidgets('empty', (t) async {
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            postListProvider(lang).overrideWithValue(
              const AsyncValue.data({}),
            ),
          ],
          child: _faker.materialWrapper(const NewsPage()),
        ),
      );
      expect(find.text('Aucune publication à afficher'), findsOneWidget);
    });

    testWidgets('with data', (t) async {
      final data = _faker.newsData();
      await t.pumpWidget(
        ProviderScope(
          overrides: [
            postListProvider(lang).overrideWithValue(
              AsyncValue.data(data),
            ),
          ],
          child: _faker.materialWrapper(const NewsPage()),
        ),
      );

      final post = data.first;
      expect(
        t.firstWidget(find.byType(PostCard)),
        isA<PostCard>()
            .having((pc) => pc.post.id, 'post.id', post.id)
            .having((pc) => pc.post.title, 'post.title', post.title),
      );

      expect(
        find.text('${post.category.toEmoji()} ${post.title}'),
        findsOneWidget,
      );
      expect(
        t.firstWidget(find.byType(MarkdownBody)),
        isA<MarkdownBody>()
            .having((md) => md.data, 'markdown.data', post.description),
      );
    });
  });
}
