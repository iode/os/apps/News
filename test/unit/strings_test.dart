import 'package:flutter_test/flutter_test.dart';
import 'package:news/shared/utils/strings.dart';

void main() {
  group('String extension methods', () {
    test('upper first empty string should be empty', () {
      expect(''.upperFirst(), '');
    });
    test('lowercase string should upper', () {
      expect('alphabet'.upperFirst(), 'Alphabet');
    });
    test('uppercase string should be the same', () {
      expect('Alphabet'.upperFirst(), 'Alphabet');
    });
    test('capitalized empty string should be empty', () {
      expect(''.capitalize(), '');
    });
    test('lowercase string should capitalize', () {
      expect('alphabet'.capitalize(), 'Alphabet');
    });
    test('lowercase sentence should capitalize', () {
      expect('14 septembre'.capitalize(), '14 Septembre');
    });
  });
}
