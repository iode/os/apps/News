import 'package:flutter_test/flutter_test.dart';
import 'package:news/shared/utils/is_equal.dart';

void main() {
  group('isEqual function', () {
    test('two empty lists should be equal', () {
      expect(isEqual([], []), true);
    });
    test('lists of different sizes should be different', () {
      expect(isEqual(['a'], ['a', 'b']), false);
    });
    test('lists test one element equal', () {
      expect(isEqual(['a'], ['a']), true);
    });
    test('lists test one element different', () {
      expect(isEqual(['a'], ['b']), false);
    });
    test('lists test two elements equal', () {
      expect(isEqual(['a', 'b'], ['a', 'b']), true);
    });
    test('lists test two elements different', () {
      expect(isEqual(['a', 'b'], ['a', 'c']), false);
    });
  });
}
