import 'dart:async';

import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart' hide Banner;
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'package:news/home/presenter/home.dart';
import 'package:news/l10n/l10n.dart';
import 'package:news/setup_wizard/presenter/setup_wizard.dart';
import 'package:news/shared/component/banner.dart';
import 'package:news/shared/data/environment.dart';
import 'package:news/shared/data/storage.dart' as s;
import 'package:news/shared/theme/theme.dart';
import 'package:news/shared/utils/polling.dart' as polling;

class App extends HookConsumerWidget {
  const App({super.key});

  Future<void> initBackgroundFetch(
    AppLifecycleState? state,
    Environment env,
  ) async {
    if (await s.isBackgroundFetchInitialized) {
      return;
    } else {
      unawaited(
        BackgroundFetch.configure(
          BackgroundFetchConfig(
            minimumFetchInterval: env.pollingFetchInterval,
            stopOnTerminate: false,
            startOnBoot: true,
            enableHeadless: true,
            requiredNetworkType: NetworkType.ANY,
          ),
          (String taskId) async {
            if (state == AppLifecycleState.inactive ||
                state == AppLifecycleState.paused) {
              await polling.onPoll(env, taskId);
            }
            BackgroundFetch.finish(taskId);
          },
        ),
      );
      unawaited(s.setBackgroundFetchInitialized());
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final state = useAppLifecycleState();
    final env = ref.watch(environmentProvider);
    initBackgroundFetch(state, env);

    return MaterialApp(
      title: 'iodé News',
      theme: ThemeData(
        useMaterial3: true,
        colorSchemeSeed: IodeTheme.darkBlue,
        brightness: Brightness.light,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        expansionTileTheme: const ExpansionTileThemeData(
          textColor: IodeTheme.darkBlue,
          iconColor: IodeTheme.darkBlue,
        ),
        switchTheme: SwitchThemeData(
          trackColor: MaterialStateProperty.resolveWith<Color?>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.selected)) {
              return IodeTheme.darkBlue;
            }
            return null;
          }),
        ),
      ),
      darkTheme: ThemeData(
        useMaterial3: true,
        colorSchemeSeed: IodeTheme.lightBlue,
        brightness: Brightness.dark,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        expansionTileTheme: const ExpansionTileThemeData(
          textColor: IodeTheme.lightBlue,
          iconColor: IodeTheme.lightBlue,
        ),
        switchTheme: SwitchThemeData(
          trackColor: MaterialStateProperty.resolveWith<Color?>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.selected)) {
              return IodeTheme.lightBlue;
            }
            return null;
          }),
        ),
      ),
      home: Banner(
        child: FutureBuilder<List<bool>>(
          future: Future.wait([s.isWelcomeModalRead, s.isSetupWizardDone]),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.hasData) {
              if (snapshot.data![0] || snapshot.data![1]) {
                return const HomePage();
              } else {
                return const SetupWizard();
              }
            }
            return const SizedBox();
          },
        ),
      ),
      localeListResolutionCallback: (locales, supportedLocales) {
        if (locales != null) {
          for (final locale in locales) {
            if (supportedLocales
                .map((supportedLocale) => supportedLocale.languageCode)
                .contains(locale.languageCode)) {
              return locale;
            }
          }
        }
        return const Locale('en');
      },
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
    );
  }
}
