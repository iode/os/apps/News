import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:news/l10n/l10n.dart';
import 'package:news/news/data/news_repository.dart';
import 'package:news/shared/data/device.dart';
import 'package:news/shared/data/environment.dart';
import 'package:news/shared/data/storage.dart' as s;
import 'package:news/shared/utils/is_equal.dart';

Future<void> onPoll(Environment env, String taskId) async {
  final isNotificationsEnabled = await s.isNotificationEnabled;
  if (isNotificationsEnabled) {
    final cachedIds = await s.postIds;
    final favoriteLocale = await s.favoriteLocale;
    final device = await getBuildProduct();
    final posts = ((await fetchPosts(env, favoriteLocale))
        .where(
          (post) => post.devices.isEmpty || post.devices.contains(device),
        )
        .toList(growable: false))
      ..sort((a, b) => b.date.compareTo(a.date));
    final postIds = posts.map((e) => e.id).toList();

    if (!isEqual(cachedIds, postIds)) {
      await displayPushNotification(favoriteLocale);
    }
  }
}

Future<void> displayPushNotification(String lang) async {
  const androidPlatformChannelSpecifics = AndroidNotificationDetails(
    'news_polling',
    'News polling',
    channelDescription: 'Polls iode news',
    importance: Importance.max,
    priority: Priority.high,
    ticker: 'ticker',
  );
  const platformChannelSpecifics = NotificationDetails(
    android: androidPlatformChannelSpecifics,
  );

  final l10n = await AppLocalizations.delegate.load(Locale(lang));
  await FlutterLocalNotificationsPlugin().show(
    0,
    l10n.newPosts,
    l10n.openApp,
    platformChannelSpecifics,
  );
}
