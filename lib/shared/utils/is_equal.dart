bool isEqual(List<String> list1, List<String> list2) {
  if (list1.length != list2.length) {
    return false;
  } else {
    for (var i = 0; i < list1.length; i++) {
      if (list1[i] != list2[i]) {
        return false;
      }
    }
  }
  return true;
}
