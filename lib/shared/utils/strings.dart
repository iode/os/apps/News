extension StringExt on String {
  /// Uppers first character in this string.
  ///
  /// ```dart
  /// 'alphabet'.upperFirst(); // 'Alphabet'
  /// ```
  String upperFirst() {
    return length < 2
        ? toUpperCase()
        : '${this[0].toUpperCase()}${substring(1)}';
  }

  /// Uppers all first characters in this string.
  ///
  /// ```dart
  /// 'alphabet is abc'.capitalize(); // 'Alphabet Is Abc'
  /// ```
  String capitalize() {
    return split(' ').isEmpty
        ? toUpperCase()
        : split(' ').map((e) => e.upperFirst()).join(' ');
  }
}
