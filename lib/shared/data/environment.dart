import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final environmentProvider = Provider<Environment>((ref) => Environment.dev);

const baseUrl = 'https://gitlab.com/iode/static/-/raw/';

enum Environment {
  prod(
    gitBranch: 'main',
    pollingFetchInterval: 1440,
  ),
  dev(
    gitBranch: 'dev',
    pollingFetchInterval: 15,
    bannerColor: Colors.blue,
  );

  const Environment({
    required this.gitBranch,
    required this.pollingFetchInterval,
    this.bannerColor = Colors.transparent,
  });

  String get postsUrl => '$baseUrl$gitBranch/posts.json';
  String get faqsUrl => '$baseUrl$gitBranch/faqs.json';

  final String gitBranch;
  final int pollingFetchInterval;
  final Color bannerColor;
}
