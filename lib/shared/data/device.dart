import 'dart:io';

import 'package:hooks_riverpod/hooks_riverpod.dart';

final buildProductProvider = FutureProvider<String>((ref) => getBuildProduct());

Future<String> getBuildProduct() async {
  final result = await Process.run('getprop', ['ro.build.product']);
  return (result.stdout as String).trim();
}
