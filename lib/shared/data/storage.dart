import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

const _postIdsKey = '___POST_IDS___';
const _favoriteLocaleKey = '___FAV_LOCALE___';
const _notificationsEnabledKey = '___NOTIFICATIONS_ENABLED___';
const _bgFetchInitializedKey = '___BACKGROUND_FETCH_INITIALIZED___';
const _welcomeModalRead = '__WELCOME_MODAL_READ__';
const _setupWizardDone = '__SETUP_WIZARD_DONE__';

// CACHED POST IDS

Future<bool> setPostIds(List<String> ids) async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.setStringList(_postIdsKey, ids);
}

Future<List<String>> get postIds async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.getStringList(_postIdsKey) ?? [];
}

// LOCALE

Future<bool> setFavoriteLocale(String locale) async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.setString(_favoriteLocaleKey, locale);
}

Future<String> get favoriteLocale async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.getString(_favoriteLocaleKey) ?? 'en';
}

// NOTIFICATIONS

Future<bool> get isNotificationEnabled async {
  final prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey(_notificationsEnabledKey)) {
    return prefs.getBool(_notificationsEnabledKey)!;
  } else {
    unawaited(prefs.setBool(_notificationsEnabledKey, true));
    return true;
  }
}

Future<bool> setNotificationEnabled({required bool enabled}) async {
  final prefs = await SharedPreferences.getInstance();
  unawaited(prefs.setBool(_notificationsEnabledKey, enabled));
  return enabled;
}

// BACKGROUND FETCH

Future<bool> get isBackgroundFetchInitialized async {
  final prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey(_bgFetchInitializedKey)) {
    return prefs.getBool(_bgFetchInitializedKey)!;
  } else {
    unawaited(prefs.setBool(_bgFetchInitializedKey, false));
    return false;
  }
}

Future<bool> setBackgroundFetchInitialized() async {
  final prefs = await SharedPreferences.getInstance();
  unawaited(prefs.setBool(_notificationsEnabledKey, true));
  return true;
}

// WELCOME MODAL

Future<bool> get isWelcomeModalRead async {
  final prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey(_welcomeModalRead)) {
    return prefs.getBool(_welcomeModalRead)!;
  } else {
    unawaited(prefs.setBool(_welcomeModalRead, false));
    return false;
  }
}

// SETUP WIZARD

Future<bool> get isSetupWizardDone async {
  final prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey(_setupWizardDone)) {
    return prefs.getBool(_setupWizardDone)!;
  } else {
    unawaited(prefs.setBool(_setupWizardDone, false));
    return false;
  }
}

Future<bool> setSetupWizardDone() async {
  final prefs = await SharedPreferences.getInstance();
  unawaited(prefs.setBool(_setupWizardDone, true));
  return true;
}

// DEBUG HELPERS

Future<bool> clear() async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.clear();
}
