/// Represents a raw internationalized element.
class I18nElement {
  I18nElement({
    required this.lang,
    required this.data,
  });

  factory I18nElement.fromJson(Map<String, dynamic> json) {
    return I18nElement(
      lang: json['lang'] as String,
      data: json['data'] as String,
    );
  }

  final String lang;
  final String data;

  @override
  String toString() => '''
{
  lang: $lang,
  data: $data,
}''';
}
