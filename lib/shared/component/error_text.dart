import 'package:flutter/material.dart';
import 'package:news/l10n/l10n.dart';

class ErrorTextWidget extends StatelessWidget {
  const ErrorTextWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(child: Text(AppLocalizations.of(context).errorOccured));
  }
}
