import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:news/shared/data/environment.dart';

class Banner extends ConsumerWidget {
  const Banner({required this.child, super.key});

  final Widget child;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final env = ref.watch(environmentProvider);

    if (env == Environment.prod) {
      return child;
    }
    return Stack(
      children: <Widget>[
        child,
        SizedBox(
          width: 50,
          height: 50,
          child: CustomPaint(
            painter: BannerPainter(
              message: env.name.toUpperCase(),
              textDirection: Directionality.of(context),
              layoutDirection: Directionality.of(context),
              location: BannerLocation.topStart,
              color: env.bannerColor,
            ),
          ),
        ),
      ],
    );
  }
}
