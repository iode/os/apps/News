import 'package:flutter/material.dart';

const _blueSwatch = {
  50: Color.fromRGBO(82, 197, 219, .1),
  100: Color.fromRGBO(82, 197, 219, .2),
  200: Color.fromRGBO(82, 197, 219, .3),
  300: Color.fromRGBO(82, 197, 219, .4),
  400: Color.fromRGBO(82, 197, 219, .5),
  500: Color.fromRGBO(82, 197, 219, .6),
  600: Color.fromRGBO(82, 197, 219, .7),
  700: Color.fromRGBO(82, 197, 219, .8),
  800: Color.fromRGBO(82, 197, 219, .9),
  900: Color.fromRGBO(82, 197, 219, 1),
};

const _lightBlueSwatch = {
  50: Color.fromRGBO(168, 226, 240, .1),
  100: Color.fromRGBO(168, 226, 240, .2),
  200: Color.fromRGBO(168, 226, 240, .3),
  300: Color.fromRGBO(168, 226, 240, .4),
  400: Color.fromRGBO(168, 226, 240, .5),
  500: Color.fromRGBO(168, 226, 240, .6),
  600: Color.fromRGBO(168, 226, 240, .7),
  700: Color.fromRGBO(168, 226, 240, .8),
  800: Color.fromRGBO(168, 226, 240, .9),
  900: Color.fromRGBO(168, 226, 240, 1),
};

const _darkBlueSwatch = {
  50: Color.fromRGBO(49, 118, 131, .1),
  100: Color.fromRGBO(49, 118, 131, .2),
  200: Color.fromRGBO(49, 118, 131, .3),
  300: Color.fromRGBO(49, 118, 131, .4),
  400: Color.fromRGBO(49, 118, 131, .5),
  500: Color.fromRGBO(49, 118, 131, .6),
  600: Color.fromRGBO(49, 118, 131, .7),
  700: Color.fromRGBO(49, 118, 131, .8),
  800: Color.fromRGBO(49, 118, 131, .9),
  900: Color.fromRGBO(49, 118, 131, 1),
};

class IodeTheme {
  IodeTheme._({
    required this.uiBlue,
    required this.bw,
    required this.bwInv,
  });

  factory IodeTheme._dark() => IodeTheme._(
        uiBlue: darkBlue,
        bw: Colors.black,
        bwInv: Colors.white,
      );

  factory IodeTheme._light() => IodeTheme._(
        uiBlue: lightBlue,
        bw: Colors.white,
        bwInv: Colors.black,
      );

  factory IodeTheme.of(BuildContext context) {
    final light = IodeTheme._light();

    final dark = IodeTheme._dark();

    return ThemeData.estimateBrightnessForColor(
              Theme.of(context).textTheme.bodyLarge!.color!,
            ) ==
            Brightness.light
        ? light
        : dark;
  }

  static const blue = MaterialColor(0xFF52C5DB, _blueSwatch);
  static const lightBlue = MaterialColor(0xFFA8E2ED, _lightBlueSwatch);
  static const darkBlue = MaterialColor(0xFF317683, _darkBlueSwatch);

  static const italic = TextStyle(fontStyle: FontStyle.italic);
  static const bold = TextStyle(fontWeight: FontWeight.bold);

  final Color uiBlue;
  final Color bw;
  final Color bwInv;
}
