import 'dart:async';

import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'package:news/app.dart';
import 'package:news/shared/data/environment.dart';
import 'package:news/shared/utils/polling.dart' as polling;

Future<void> main({Environment env = Environment.dev}) async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    ProviderScope(
      overrides: [
        environmentProvider.overrideWithValue(env),
      ],
      child: const App(),
    ),
  );

  // Init local notifications
  await FlutterLocalNotificationsPlugin().initialize(
    const InitializationSettings(
      android: AndroidInitializationSettings('app_icon'),
    ),
  );

  await FlutterLocalNotificationsPlugin()
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.requestPermission();

  env == Environment.dev
      ? unawaited(
          BackgroundFetch.registerHeadlessTask(backgroundFetchHeadlessTaskDev),
        )
      : unawaited(
          BackgroundFetch.registerHeadlessTask(
            backgroundFetchHeadlessTaskProd,
          ),
        );
}

@pragma('vm:entry-point')
Future<void> backgroundFetchHeadlessTaskDev(HeadlessTask task) async {
  await backgroundFetchHeadlessTask(task, Environment.dev);
}

@pragma('vm:entry-point')
Future<void> backgroundFetchHeadlessTaskProd(HeadlessTask task) async {
  await backgroundFetchHeadlessTask(task, Environment.prod);
}

Future<void> backgroundFetchHeadlessTask(
  HeadlessTask task,
  Environment env,
) async {
  final taskId = task.taskId;
  final isTimeout = task.timeout;
  if (isTimeout) {
    // This task has exceeded its allowed running-time.
    // You must stop what you're doing and immediately .finish(taskId)
    BackgroundFetch.finish(taskId);
    return;
  }
  await polling.onPoll(env, taskId);
  BackgroundFetch.finish(taskId);
}
