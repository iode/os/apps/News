import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:news/shared/data/storage.dart' as s;

final notificationsProvider = StateNotifierProvider<Notifications, bool>(
  (ref) => Notifications()..load(),
);

class Notifications extends StateNotifier<bool> {
  Notifications() : super(true);

  Future<void> load() async {
    final isNotificationEnabled = await s.isNotificationEnabled;
    state = isNotificationEnabled;
  }

  Future<void> set({required bool enabled}) async {
    final isNotificationEnabled =
        await s.setNotificationEnabled(enabled: enabled);
    state = isNotificationEnabled;
  }
}
