import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:news/l10n/l10n.dart';
import 'package:news/settings/presenter/provider.dart';

class SettingsPage extends ConsumerWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final l10n = AppLocalizations.of(context);
    final notificationsEnabled = ref.watch(notificationsProvider);
    return Scaffold(
      appBar: AppBar(
        title: Text(l10n.settings),
      ),
      body: Column(
        children: [
          ListTile(
            title: Text(l10n.notifsOnOff),
            trailing: Switch(
              value: notificationsEnabled,
              onChanged: (value) =>
                  ref.read(notificationsProvider.notifier).set(enabled: value),
            ),
            subtitle: Text(
              notificationsEnabled ? l10n.notifsOn : l10n.notifsOff,
            ),
            onTap: () => ref
                .read(notificationsProvider.notifier)
                .set(enabled: !notificationsEnabled),
          ),
        ],
      ),
    );
  }
}
