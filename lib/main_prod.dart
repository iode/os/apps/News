import 'package:news/main.dart' as app;
import 'package:news/shared/data/environment.dart';

void main() {
  app.main(env: Environment.prod);
}
