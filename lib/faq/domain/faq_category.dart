import 'package:flutter/widgets.dart';

import 'package:news/l10n/l10n.dart';

enum FAQCategory {
  about,
  general,
  technical,
  other;

  String pretty(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    switch (this) {
      case FAQCategory.about:
        return l10n.about;
      case FAQCategory.general:
        return l10n.generalQuestions;
      case FAQCategory.technical:
        return l10n.technicalQuestions;
      case FAQCategory.other:
        return l10n.others;
    }
  }
}
