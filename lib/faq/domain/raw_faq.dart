import 'package:news/faq/domain/faq.dart';
import 'package:news/faq/domain/faq_category.dart';
import 'package:news/shared/entities/i18n_element.dart';

class RawFAQ {
  RawFAQ({
    required this.devices,
    required this.category,
    required this.title,
    required this.description,
  });

  factory RawFAQ.fromJson(Map<String, dynamic> json) {
    return RawFAQ(
      devices: json['device'] == null
          ? []
          : List<String>.from(json['device'] as List<dynamic>),
      category: json['category'] as String,
      title: List<I18nElement>.from(
        (json['title'] as List<dynamic>).map<I18nElement>(
          (dynamic x) => I18nElement.fromJson(x as Map<String, dynamic>),
        ),
      ),
      description: List<I18nElement>.from(
        (json['description'] as List<dynamic>).map<I18nElement>(
          (dynamic x) => I18nElement.fromJson(x as Map<String, dynamic>),
        ),
      ),
    );
  }

  final List<String> devices;
  final String category;
  final List<I18nElement> title;
  final List<I18nElement> description;
}

extension FAQExtension on RawFAQ {
  FAQ toFaq(String lang) {
    return FAQ(
      category: category.fromString(),
      devices: devices,
      title: title.firstWhere((element) => element.lang == lang).data,
      description: description.firstWhere((e) => e.lang == lang).data,
    );
  }
}

extension on String {
  FAQCategory fromString() {
    switch (this) {
      case 'about':
        return FAQCategory.about;
      case 'general':
        return FAQCategory.general;
      case 'technical':
        return FAQCategory.technical;
      default:
        return FAQCategory.other;
    }
  }
}
