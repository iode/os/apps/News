import 'package:news/faq/domain/faq_category.dart';

class FAQ {
  FAQ({
    required this.category,
    required this.devices,
    required this.title,
    required this.description,
  });

  final FAQCategory category;
  final List<String> devices;
  final String title;
  final String description;

  @override
  String toString() => '''
{
  category: $category,
  title: $title,
  description: $description,
  devices: $devices
}
''';
}
