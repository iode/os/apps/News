import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:news/faq/data/faq_repository.dart';
import 'package:news/faq/domain/faq.dart';
import 'package:news/faq/domain/faq_category.dart';
import 'package:news/shared/data/device.dart';
import 'package:news/shared/data/environment.dart';

final faqListProvider =
    FutureProvider.family<Map<FAQCategory, Set<FAQ>>, String>(
        (ref, lang) async {
  final device = await ref.watch(buildProductProvider.future);
  final env = ref.watch(environmentProvider);
  final questions = (await fetchFAQ(env, lang))
      .where((q) => q.devices.isEmpty || q.devices.contains(device))
      .toSet();
  final map = <FAQCategory, Set<FAQ>>{};
  for (final cat in FAQCategory.values) {
    final relatedQuestions = questions.where((q) => q.category == cat).toSet();
    if (relatedQuestions.isNotEmpty) {
      map.addAll({cat: relatedQuestions});
    }
  }
  return map;
});
