import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:news/faq/domain/faq.dart';
import 'package:news/faq/domain/faq_category.dart';
import 'package:news/faq/presenter/provider.dart';
import 'package:news/l10n/l10n.dart';
import 'package:news/shared/component/error_text.dart';
import 'package:news/shared/component/no_connection.dart';
import 'package:url_launcher/url_launcher.dart';

class FAQPage extends ConsumerWidget {
  const FAQPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final l10n = AppLocalizations.of(context);
    final lang = l10n.localeName;
    final faqs = ref.watch(faqListProvider(lang));
    return Scaffold(
      appBar: AppBar(title: Text(l10n.faq)),
      body: faqs.when(
        error: (err, _) => err is SocketException
            ? const NoConnectionWidget()
            : const ErrorTextWidget(),
        loading: () => const Center(child: CircularProgressIndicator()),
        data: (faqs) => faqs.isEmpty
            ? Center(child: Text(l10n.noFaq))
            : ListView.builder(
                itemCount: faqs.length,
                itemBuilder: (_, i) => FaqTile(
                  category: faqs.keys.elementAt(i),
                  questions: faqs.values.elementAt(i),
                ),
              ),
      ),
    );
  }
}

class FaqTile extends StatelessWidget {
  const FaqTile({
    required this.category,
    required this.questions,
    super.key,
  });

  final FAQCategory category;
  final Set<FAQ> questions;

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    return ExpansionTile(
      title: Text(
        category.pretty(context),
        style: const TextStyle(fontSize: 18),
      ),
      children: [
        if (questions.isEmpty) ListTile(title: Text(l10n.noQuestion)),
        if (questions.isNotEmpty)
          for (final q in questions)
            Padding(
              padding: const EdgeInsets.only(left: 8, top: 8, bottom: 8),
              child: ExpansionTile(
                key: Key(q.title),
                title: Text(q.title),
                childrenPadding: const EdgeInsets.all(8),
                children: [
                  MarkdownBody(
                    data: q.description,
                    selectable: true,
                    onTapLink: (text, href, title) async {
                      if (href != null && await canLaunchUrl(Uri.parse(href))) {
                        await launchUrl(Uri.parse(href));
                      }
                    },
                  ),
                ],
              ),
            ),
      ],
    );
  }
}
