import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:news/faq/domain/faq.dart';
import 'package:news/faq/domain/raw_faq.dart';
import 'package:news/shared/data/environment.dart';

Future<Set<FAQ>> fetchFAQ(Environment env, String lang) async {
  try {
    final response = await http.get(Uri.parse(env.faqsUrl));
    final rawFaqs = Set<RawFAQ>.from(
      (json.decode(response.body) as List<dynamic>).map<RawFAQ>(
        (dynamic e) => RawFAQ.fromJson(e as Map<String, dynamic>),
      ),
    );
    return rawFaqs
        .where(
          (e) =>
              e.title.any((e) => e.lang == lang) &&
              e.description.any((e) => e.lang == lang),
        )
        .map((e) => e.toFaq(lang))
        .toSet();
  } catch (e, s) {
    return Future.error(e, s);
  }
}
