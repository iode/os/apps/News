import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:news/l10n/l10n.dart';
import 'package:url_launcher/url_launcher.dart';

class GetInvolvedPage extends StatelessWidget {
  const GetInvolvedPage({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    final getInvolvedElements = {
      l10n.joinCommunity: l10n.joinCommunityContent,
      l10n.joinBeta: l10n.joinBetaContent,
      l10n.reportIssue: l10n.reportIssueContent,
      l10n.buyCoffee: l10n.buyCoffeeContent,
    };

    return Scaffold(
      appBar: AppBar(title: Text(l10n.getInvolved)),
      body: ListView(
        children: [
          for (final k in getInvolvedElements.keys)
            ExpansionTile(
              title: Text(k),
              children: [
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: MarkdownBody(
                    data: getInvolvedElements[k]!,
                    onTapLink: (text, href, title) async {
                      if (href != null && await canLaunchUrl(Uri.parse(href))) {
                        await launchUrl(
                          Uri.parse(href),
                          mode: LaunchMode.externalApplication,
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
        ],
      ),
    );
  }
}
