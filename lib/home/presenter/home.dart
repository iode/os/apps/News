import 'package:flutter/material.dart';

import 'package:news/faq/presenter/faq.dart';
import 'package:news/get_involved/presenter/get_involved.dart';
import 'package:news/l10n/l10n.dart';
import 'package:news/news/presenter/news.dart';

const kTabHeight = 46.0;
const kIndicatorHeight = 2.0;

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    final tabs = {
      const Tab(text: 'NEWS'): const NewsPage(),
      Tab(text: l10n.getInvolved.toUpperCase()): const GetInvolvedPage(),
      const Tab(text: 'FAQ'): const FAQPage(),
    };

    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        body: TabBarView(
          children: tabs.values.toList(),
        ),
        bottomNavigationBar: TabBar(
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(
              color: Theme.of(context).indicatorColor,
              width: kIndicatorHeight,
            ),
            insets: const EdgeInsets.fromLTRB(
              0,
              0,
              0,
              kTabHeight + kIndicatorHeight,
            ),
          ),
          labelColor:
              MediaQuery.of(context).platformBrightness == Brightness.light
                  ? Colors.black87
                  : Colors.white,
          tabs: tabs.keys.toList(),
        ),
      ),
    );
  }
}
