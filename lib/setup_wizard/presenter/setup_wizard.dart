import 'package:device_apps/device_apps.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import 'package:news/home/presenter/home.dart';
import 'package:news/l10n/l10n.dart';
import 'package:news/shared/data/storage.dart' as s;
import 'package:news/shared/theme/theme.dart';

import 'package:url_launcher/url_launcher.dart';

class SetupWizard extends HookWidget {
  const SetupWizard({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    final controller = useAnimationController(
      duration: const Duration(seconds: 4),
    );
    final doubleTween = Tween<double>(begin: 0, end: 1);
    final welcomeAnimation = useAnimation(
      doubleTween.animate(
        CurvedAnimation(
          parent: controller,
          curve: const Interval(0.1, 0.2, curve: Curves.easeIn),
        ),
      ),
    );
    final startAnimation = useAnimation(
      doubleTween.animate(
        CurvedAnimation(
          parent: controller,
          curve: const Interval(0.9, 1, curve: Curves.ease),
        ),
      ),
    );

    controller.forward();

    s.setSetupWizardDone();

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Opacity(
              opacity: welcomeAnimation,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 32),
                child: FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Text(
                    l10n.welcomeToIode,
                    style: Theme.of(context)
                        .textTheme
                        .displayMedium!
                        .copyWith(color: IodeTheme.of(context).uiBlue),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            FadeTransition(
              opacity: doubleTween.animate(
                CurvedAnimation(
                  parent: controller,
                  curve: const Interval(0.4, 0.5, curve: Curves.ease),
                ),
              ),
              child: _SetupCard(
                text: l10n.selectPreinstalledApps,
                onPressed: () => launchUrl(Uri.parse('iode://setup')),
              ),
            ),
            FadeTransition(
              opacity: doubleTween.animate(
                CurvedAnimation(
                  parent: controller,
                  curve: const Interval(0.5, 0.6, curve: Curves.ease),
                ),
              ),
              child: _SetupCard(
                text: l10n.discoverIodeBlocker,
                onPressed: () => DeviceApps.openApp('com.iode.app'),
              ),
            ),
            FadeTransition(
              opacity: doubleTween.animate(
                CurvedAnimation(
                  parent: controller,
                  curve: const Interval(0.6, 0.7, curve: Curves.ease),
                ),
              ),
              child: _SetupCard(
                text: l10n.checkNews,
                onPressed: () => Navigator.push<dynamic>(
                  context,
                  MaterialPageRoute<dynamic>(builder: (_) => const HomePage()),
                ),
              ),
            ),
            Opacity(
              opacity: startAnimation,
              child: Align(
                alignment: Alignment.bottomRight,
                child: ElevatedButton(
                  onPressed: SystemNavigator.pop,
                  style: ElevatedButton.styleFrom(
                    foregroundColor: IodeTheme.of(context).bwInv,
                    backgroundColor: IodeTheme.of(context).uiBlue,
                  ),
                  child: Text(l10n.letsGo),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _SetupCard extends StatelessWidget {
  const _SetupCard({
    required this.text,
    required this.onPressed,
  });

  final String text;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onPressed,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 32),
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.titleLarge,
          ),
        ),
      ),
    );
  }
}
