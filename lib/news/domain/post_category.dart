enum PostCategory {
  news('🔔'),
  update('🚀'),
  bug('🐛'),
  init('🎉'),
  poll('🎤');

  const PostCategory(this.emoji);

  final String emoji;
}
