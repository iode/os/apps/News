import 'package:collection/collection.dart';
import 'package:news/news/domain/post.dart';
import 'package:news/news/domain/post_category.dart';
import 'package:news/shared/entities/i18n_element.dart';

class RawPost {
  RawPost({
    required this.id,
    required this.devices,
    required this.category,
    required this.title,
    required this.description,
    required this.date,
  });

  factory RawPost.fromJson(Map<String, dynamic> json) {
    return RawPost(
      id: json['id'] as String,
      devices: json['device'] == null
          ? []
          : List<String>.from(json['device'] as List<dynamic>),
      category: json['category'] as String,
      title: List<I18nElement>.from(
        (json['title'] as List<dynamic>).map<I18nElement>(
          (dynamic x) => I18nElement.fromJson(x as Map<String, dynamic>),
        ),
      ),
      description: List<I18nElement>.from(
        (json['description'] as List<dynamic>).map<I18nElement>(
          (dynamic x) => I18nElement.fromJson(x as Map<String, dynamic>),
        ),
      ),
      date: json['date'] as String,
    );
  }

  final String id;
  final List<String> devices;
  final String category;
  final List<I18nElement> title;
  final List<I18nElement> description;
  final String date;

  @override
  String toString() => '''
{
  id: $id,
  devices: $devices,
  category: $category,
  title: $title,
  description: $description,
  date: $date,  
}''';
}

extension PostExtension on RawPost {
  Post toPost(String lang) {
    final titleTr =
        (title.firstWhereOrNull((element) => element.lang == lang) ??
                title.firstWhere((e) => e.lang == 'en'))
            .data;
    final descTr =
        (description.firstWhereOrNull((element) => element.lang == lang) ??
                description.firstWhere((e) => e.lang == 'en'))
            .data;
    return Post(
      id: id,
      category: category.fromString(),
      devices: devices,
      title: titleTr,
      description: descTr,
      date: DateTime.parse(date).toLocal(),
    );
  }
}

extension on String {
  PostCategory fromString() {
    switch (this) {
      case 'news':
        return PostCategory.news;
      case 'update':
        return PostCategory.update;
      case 'poll':
        return PostCategory.poll;
      case 'bug':
        return PostCategory.bug;
      case 'init':
        return PostCategory.init;
      default:
        throw Exception('Category $this not supported');
    }
  }
}
