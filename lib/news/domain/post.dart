import 'package:news/news/domain/post_category.dart';

class Post {
  Post({
    required this.id,
    required this.category,
    required this.devices,
    required this.title,
    required this.description,
    required this.date,
  });

  final String id;
  final PostCategory category;
  final DateTime date;
  final String? description;
  final List<String> devices;
  final String title;

  @override
  String toString() => '''
{
  category: $category,
  title: $title,
  description: $description
}
''';
}
