import 'dart:collection';

import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:news/news/data/news_repository.dart';
import 'package:news/news/domain/post.dart';
import 'package:news/shared/data/device.dart';
import 'package:news/shared/data/environment.dart';
import 'package:news/shared/data/storage.dart' as s;

final postListProvider =
    FutureProvider.family<Set<Post>, String>((ref, lang) async {
  await s.setFavoriteLocale(lang);
  final env = ref.watch(environmentProvider);
  final device = await ref.watch(buildProductProvider.future);
  var posts = (await fetchPosts(env, lang))
      .where((post) => post.devices.isEmpty || post.devices.contains(device))
      .toSet();
  posts = SplayTreeSet.from(posts, (a, b) => b.date.compareTo(a.date));
  await s.setPostIds([...posts.map((p) => p.id)]);
  return posts;
});
