import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'package:news/l10n/l10n.dart';
import 'package:news/news/presenter/post_card.dart';
import 'package:news/news/presenter/provider.dart';
import 'package:news/settings/presenter/settings.dart';
import 'package:news/shared/component/error_text.dart';
import 'package:news/shared/component/no_connection.dart';
import 'package:news/shared/data/storage.dart' as s;

class NewsPage extends ConsumerWidget {
  const NewsPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final l10n = AppLocalizations.of(context);
    final lang = l10n.localeName;
    final posts = ref.watch(postListProvider(lang));
    return Scaffold(
      appBar: AppBar(
        title: const Text('News'),
        actions: <Widget>[
          if (kDebugMode)
            const IconButton(
              onPressed: s.clear,
              icon: Icon(Icons.delete),
              tooltip: 'Clear storage',
            ),
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () => Navigator.push<dynamic>(
              context,
              MaterialPageRoute<dynamic>(builder: (_) => const SettingsPage()),
            ),
          ),
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () => ref.refresh(postListProvider(lang).future),
          ),
        ],
      ),
      body: posts.when(
        error: (err, _) => err is SocketException
            ? const NoConnectionWidget()
            : const ErrorTextWidget(),
        loading: () => const Center(child: CircularProgressIndicator()),
        data: (posts) => posts.isEmpty
            ? Center(child: Text(l10n.noPosts))
            : ListView.builder(
                itemCount: posts.length,
                itemBuilder: (_, i) => PostCard(post: posts.elementAt(i)),
                reverse: true,
              ),
      ),
    );
  }
}
