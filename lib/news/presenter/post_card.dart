import 'package:device_apps/device_apps.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:intl/intl.dart';
import 'package:news/news/domain/post.dart';
import 'package:news/news/domain/post_category.dart';
import 'package:news/shared/theme/theme.dart';
import 'package:news/shared/utils/strings.dart';
import 'package:url_launcher/url_launcher.dart';

class PostCard extends StatelessWidget {
  const PostCard({
    required this.post,
    super.key,
  });

  final Post post;

  @override
  Widget build(BuildContext context) {
    final lang = Localizations.localeOf(context).languageCode;
    return Column(
      children: [
        Chip(
          label: Text(
            post.date.year < DateTime.now().year
                ? DateFormat.yMMMMd(lang).format(post.date).capitalize()
                : DateFormat.MMMMd(lang).format(post.date).capitalize(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8),
          child: Card(
            margin: const EdgeInsets.only(bottom: 8, left: 8, right: 8),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _Title(label: post.title, category: post.category),
                  _Body(content: post.description ?? ''),
                  _Time(lang: lang, date: post.date)
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _Title extends StatelessWidget {
  const _Title({
    required this.label,
    required this.category,
  });

  final String? label;
  final PostCategory category;

  @override
  Widget build(BuildContext context) {
    return label == null
        ? const SizedBox()
        : Padding(
            padding: const EdgeInsets.only(top: 12),
            child: Text(
              '${category.emoji} $label',
              style: TextStyle(
                color: IodeTheme.of(context).uiBlue,
                fontWeight: FontWeight.bold,
              ),
            ),
          );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    required this.content,
  });

  final String content;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: MarkdownBody(
        data: content,
        selectable: true,
        onTapLink: (_, url, __) async {
          if (url == null) {
            return;
          } else if (url == 'updater') {
            await DeviceApps.openApp('org.lineageos.updater');
          } else if (await canLaunchUrl(Uri.parse(url))) {
            await launchUrl(
              Uri.parse(url),
              mode: LaunchMode.externalApplication,
            );
          }
        },
      ),
    );
  }
}

class _Time extends StatelessWidget {
  const _Time({
    required this.lang,
    required this.date,
  });

  final String lang;
  final DateTime date;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8, right: 8),
      child: Align(
        alignment: Alignment.bottomRight,
        child: Text(
          DateFormat.Hm(lang).format(date),
          style: TextStyle(
            color: Colors.grey.shade500,
          ),
        ),
      ),
    );
  }
}
