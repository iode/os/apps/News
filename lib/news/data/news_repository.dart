import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:news/news/domain/post.dart';
import 'package:news/news/domain/raw_post.dart';
import 'package:news/shared/data/environment.dart';

Future<Set<Post>> fetchPosts(Environment env, String lang) async {
  try {
    final response = await http.get(Uri.parse(env.postsUrl));
    final rawPosts = Set<RawPost>.from(
      (json.decode(response.body) as List<dynamic>).map<RawPost>(
        (dynamic e) => RawPost.fromJson(e as Map<String, dynamic>),
      ),
    );
    return rawPosts.map((e) => e.toPost(lang)).toSet();
  } catch (e, s) {
    return Future.error(e, s);
  }
}
